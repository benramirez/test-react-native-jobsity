import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ImageBackground,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../utils/colors';

const EpisodeDetail = ({route}: any) => {
  const {episode} = route.params;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <ImageBackground
        source={
          episode.image
            ? {uri: episode.image.original}
            : require('../../assets/images/image.png')
        }
        resizeMode="cover"
        style={styles.image}>
        <LinearGradient
          colors={[
            'rgba(22, 24, 28, 0)',
            'rgba(22, 24, 28, .5)',
            'rgba(22, 24, 28, 1)',
          ]}
          style={styles.linearGradient}>
          <Text style={styles.title}>{episode.name}</Text>
          <Text
            style={
              styles.seasonText
            }>{`Season ${episode.season} | Episode ${episode.number}`}</Text>
          <Text style={styles.seasonText}>{`${episode.airdate}`}</Text>
          <Text style={styles.seasonText}>{`${episode.runtime} min `}</Text>
        </LinearGradient>
      </ImageBackground>
      <View style={styles.summaryView}>
        <Text style={styles.subtitle}>Summary:</Text>
        {episode.summary ? (
          <Text style={styles.summary}>
            {episode.summary.replace(/<[^>]+>/g, '')}
          </Text>
        ) : (
          <Text style={styles.summary}>
            La descripción no está disponile por el momento.
          </Text>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#1d1e24',
    flexGrow: 1,
  },
  image: {
    height: 300,
    width: '100%',
  },
  linearGradient: {
    alignItems: 'flex-start',
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 10,
    paddingHorizontal: 20,
  },
  seasonText: {
    color: '#d3d3d3',
    fontSize: 14,
    marginBottom: 5,
  },
  subtitle: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  summary: {
    color: '#d3d3d3',
    fontSize: 16,
    lineHeight: 24,
    marginBottom: 10,
    marginTop: 10,
  },
  summaryView: {
    marginBottom: 20,
    padding: 20,
    width: '100%',
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  title: {
    color: colors.white,
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

export default EpisodeDetail;
