import {call, put, takeLatest} from 'redux-saga/effects';
import {
  FETCH_SHOWS_REQUEST,
  fetchShowsSuccess,
  fetchShowsFailure,
  SEARCH_SHOWS_REQUEST,
  searchShowsSuccess,
  searchShowsFailure,
  FETCH_SEASONS_REQUEST,
  fetchSeasonsSuccess,
  fetchSeasonsFailure,
  FETCH_PAGE_REQUEST,
  fetchPageSuccess,
  fetchPageFailure,
} from '../actions/showsActions';
import {axiosInstance} from '../store';

function* fetchSeasons(action: any) {
  try {
    const {showId} = action.payload;
    const response = yield call(axiosInstance.get, `/shows/${showId}/episodes`);
    const seasons: any = {};
    response.data.forEach((episode: any) => {
      const seasonNumber = episode.season;
      if (!seasons[seasonNumber]) {
        seasons[seasonNumber] = [];
      }
      seasons[seasonNumber].push(episode);
    });
    const seasonsArray = Object.values(seasons);
    yield put(fetchSeasonsSuccess(seasonsArray));
  } catch (error) {
    yield put(fetchSeasonsFailure(error));
  }
}

function* fetchShows() {
  try {
    const response = yield call(axiosInstance.get, `/shows`);
    yield put(fetchShowsSuccess(response.data));
  } catch (error) {
    yield put(fetchShowsFailure(error));
  }
}

function* fetchPages(action: any) {
  try {
    const {page} = action.payload;
    const response = yield call(axiosInstance.get, `/shows?page=${page}`);
    yield put(fetchPageSuccess(response.data));
  } catch (error) {
    yield put(fetchPageFailure(error));
  }
}

function* searchShows(action: any) {
  try {
    const {query} = action.payload;
    const response = yield call(axiosInstance.get, `/search/shows?q=${query}`);
    yield put(searchShowsSuccess(response.data.map(item => item.show)));
  } catch (error) {
    yield put(searchShowsFailure(error));
  }
}

export function* watchFetchShows() {
  yield takeLatest(FETCH_SHOWS_REQUEST, fetchShows);
  yield takeLatest(SEARCH_SHOWS_REQUEST, searchShows);
  yield takeLatest(FETCH_SEASONS_REQUEST, fetchSeasons);
  yield takeLatest(FETCH_PAGE_REQUEST, fetchPages);
}
