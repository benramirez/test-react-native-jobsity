import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import colors from '../../utils/colors';

const LoadingFull = () => {
  return (
    <View style={styles.imageContainer}>
      <ActivityIndicator
        style={StyleSheet.absoluteFill}
        size="large"
        color={colors.primary}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1d1e24',
  },
});

export default LoadingFull;
