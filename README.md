
## Usage
Once the app is installed and running, you can:

- Browse and search for TV series
- View series details including episodes
- Set a PIN number for security (Bonus feature)
- Enable fingerprint authentication (Bonus feature)
- Save series as favorites (Bonus feature)

## Mandatory Features

- [x] **List all of the series contained in the API used by the paging scheme provided by the API.**
- [x] **Allow users to search series by name.**
- [x] **The listing and search views must show at least the name and poster image of the series.**
- [x] **After clicking on a series, the application should show the details of the series, showing the following information:**
Name, Poster, Days and time during which the series airs, Genres, Summary, List of episodes separated by season
- [x] **After clicking on an episode, the application should show the episode’s information, including:** Name, Number, Season, Summary, Image, if there is one

## Bonus (Optional)

- [ ] Allow the user to set a PIN number to secure the application and prevent unauthorized users.
- [ ] For supported phones, the user must be able to choose if they want to enable fingerprint authentication to avoid typing the PIN number while opening the app.
- [x] Allow the user to save a series as a favorite.
- [x] Allow the user to delete a series from the favorites list.
- [x] Allow the user to browse their favorite series in alphabetical order, and click on one to see its details.
- [ ] Create a people search by listing the name and image of the person.
- [ ] After clicking on a person, the application should show the details of that person, such as: Name, Image, Series they have participated in, with a link to the series details.

## Screenshots

![Icon](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291035.png)

![Home Screen](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291322.png)

![Menu](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291329.png)

![Series Details](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291333.png)

![Seasons](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291339.png)

![Episode Detail](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291361.png)

![Search Screen](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291373.png)

![Favorites screen](https://gitlab.com/benramirez/test-react-native-jobsity/-/raw/main/Screenshots/Screenshot_1712291384.png)

## Technologies Used
- React Native: 0.73.6
- React: 18.2.0
- @react-navigation/native: 7.0.0-alpha.18
- @react-navigation/stack: 7.0.0-alpha.20
- @react-redux: 9.1.0
- Redux: 5.0.1
- Redux Saga: 1.3.0
- Axios: 1.6.8
- Lodash: 4.17.21
- @react-native-async-storage/async-storage: 1.23.1
- @react-native-community/masked-view: 0.1.11
- @react-native-picker/picker: 2.7.2
- react-native-gesture-handler: 2.16.0
- react-native-linear-gradient: 2.8.3
- react-native-reanimated: 3.8.1
- react-native-safe-area-context: 4.9.0
- react-native-screens: 3.30.1
- react-native-svg: 15.1.0
- react-native-svg-transformer: 1.3.0
- phosphor-react-native: 2.0.0

## Contribution
Contributions to this project are welcome! If you would like to contribute, please fork the repository and create a pull request with your changes.

## Credits
- TVMaze API for providing TV series data
- Icons from Phosphor React Native

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact
For any inquiries or feedback, feel free to contact us at [contactobenramirez@gmail.com](mailto:contactobenramirez@gmail.com).
