import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  fetchShowsRequest,
  searchShowsRequest,
} from '../../redux/actions/showsActions';
import {AppState} from '../../../src/redux/store';
import LoadingImage from '../common/LoadingImage';
import LoadingFull from '../common/LoadingFull';
import {useNavigation} from '@react-navigation/native';
import colors from '../../utils/colors';
import {StackNavigationProp} from '@react-navigation/stack';

const SearchScreen = () => {
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();
  const [query, setQuery] = useState('');
  const {shows, loading, error} = useSelector((state: AppState) => state.shows);
  const [typingTimeout, setTypingTimeout] = useState<NodeJS.Timeout | null>(
    null,
  );

  useEffect(() => {
    dispatch(fetchShowsRequest());
  }, [dispatch]);

  const handleTextInputChange = (text: string) => {
    setQuery(text);

    if (typingTimeout) {
      clearTimeout(typingTimeout);
    }

    const timeoutId = setTimeout(() => {
      if (text !== '') {
        dispatch(searchShowsRequest({query: text}));
      } else {
        dispatch(fetchShowsRequest());
      }
    }, 1000);

    setTypingTimeout(timeoutId);
  };

  const renderItem = ({item}: any) => {
    return (
      <View style={styles.renderView}>
        <TouchableOpacity
          onPress={() => {
            navigation.push('SerieDetail', {show: item});
          }}
          style={styles.itemContainer}>
          {item.image && (
            <LoadingImage
              source={{uri: item.image.medium}}
              style={styles.itemImage}
            />
          )}
          <Text style={styles.itemText}>{item.name}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        value={query}
        onChangeText={handleTextInputChange}
        placeholder="Search shows..."
      />
      {loading ? (
        <LoadingFull />
      ) : error ? (
        <Text>Error: {error.message}</Text>
      ) : (
        <FlatList
          numColumns={3}
          data={shows}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#1d1e24',
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  input: {
    backgroundColor: colors.white,
    borderColor: 'gray',
    borderRadius: 10,
    borderWidth: 1,
    height: 40,
    marginBottom: 20,
    paddingHorizontal: 10,
    width: '100%',
  },
  itemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemImage: {
    height: '100%',
    width: '100%',
  },
  itemText: {
    backgroundColor: 'rgba(0,0,0,.5)',
    bottom: 5,
    borderRadius: 10,
    color: colors.white,
    fontSize: 10,
    padding: 3,
    position: 'absolute',
    textAlign: 'center',
    zIndex: 10,
  },
  renderView: {
    borderRadius: 10,
    height: 160,
    overflow: 'hidden',
    padding: 2,
    width: '33.3%',
  },
});

export default SearchScreen;
