import {
  FETCH_SHOWS_REQUEST,
  FETCH_SHOWS_SUCCESS,
  FETCH_SHOWS_FAILURE,
  SEARCH_SHOWS_REQUEST,
  SEARCH_SHOWS_SUCCESS,
  SEARCH_SHOWS_FAILURE,
  FETCH_SEASONS_REQUEST,
  FETCH_SEASONS_SUCCESS,
  FETCH_SEASONS_FAILURE,
  FETCH_PAGE_REQUEST,
  FETCH_PAGE_SUCCESS,
  FETCH_PAGE_FAILURE,
} from '../actions/showsActions';
import {AnyAction} from 'redux';
export interface ShowsState {
  shows: any[];
  showsPages: any[];
  seasons: {};
  loading: boolean;
  error: any;
  test: string;
}

const initialState: ShowsState = {
  shows: [],
  showsPages: [],
  seasons: {},
  loading: false,
  error: null,
  test: 'mensjae de prueba',
};

const showsReducer = (state = initialState, action: AnyAction): ShowsState => {
  switch (action.type) {
    case FETCH_SHOWS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_SHOWS_SUCCESS:
      return {
        ...state,
        shows: action.payload,
        loading: false,
        error: null,
      };
    case FETCH_SHOWS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case FETCH_PAGE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PAGE_SUCCESS:
      return {
        ...state,
        showsPages: action.payload,
        loading: false,
        error: null,
      };
    case FETCH_PAGE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case SEARCH_SHOWS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case SEARCH_SHOWS_SUCCESS:
      return {
        ...state,
        shows: action.payload,
        loading: false,
        error: null,
      };
    case SEARCH_SHOWS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case FETCH_SEASONS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_SEASONS_SUCCESS:
      return {
        ...state,
        seasons: action.payload,
        loading: false,
        error: null,
      };
    case FETCH_SEASONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
};

export default showsReducer;
