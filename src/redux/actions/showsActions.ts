export const FETCH_SHOWS_REQUEST = 'FETCH_SHOWS_REQUEST';
export const FETCH_SHOWS_SUCCESS = 'FETCH_SHOWS_SUCCESS';
export const FETCH_SHOWS_FAILURE = 'FETCH_SHOWS_FAILURE';
export const SEARCH_SHOWS_REQUEST = 'SEARCH_SHOWS_REQUEST';
export const SEARCH_SHOWS_SUCCESS = 'SEARCH_SHOWS_SUCCESS';
export const SEARCH_SHOWS_FAILURE = 'SEARCH_SHOWS_FAILURE';
export const FETCH_SEASONS_REQUEST = 'FETCH_SEASONS_REQUEST';
export const FETCH_SEASONS_SUCCESS = 'FETCH_SEASONS_SUCCESS';
export const FETCH_SEASONS_FAILURE = 'FETCH_SEASONS_FAILURE';
export const FETCH_PAGE_REQUEST = 'FETCH_PAGE_REQUEST';
export const FETCH_PAGE_SUCCESS = 'FETCH_PAGE_SUCCESS';
export const FETCH_PAGE_FAILURE = 'FETCH_PAGE_FAILURE';

export const fetchShowsRequest = () => ({
  type: FETCH_SHOWS_REQUEST,
});

export const fetchShowsSuccess = (shows: any) => ({
  type: FETCH_SHOWS_SUCCESS,
  payload: shows,
});

export const fetchShowsFailure = (error: any) => ({
  type: FETCH_SHOWS_FAILURE,
  payload: error,
});

export const fetchPageRequest = (payload: any) => ({
  type: FETCH_PAGE_REQUEST,
  payload: payload,
});

export const fetchPageSuccess = (shows: any) => ({
  type: FETCH_PAGE_SUCCESS,
  payload: shows,
});

export const fetchPageFailure = (error: any) => ({
  type: FETCH_PAGE_FAILURE,
  payload: error,
});

export const searchShowsRequest = (payload: string) => ({
  type: SEARCH_SHOWS_REQUEST,
  payload: payload,
});

export const searchShowsSuccess = (shows: any[]) => ({
  type: SEARCH_SHOWS_SUCCESS,
  payload: shows,
});

export const searchShowsFailure = (error: any) => ({
  type: SEARCH_SHOWS_FAILURE,
  payload: error,
});

export const fetchSeasonsRequest = (showId: number) => ({
  type: FETCH_SEASONS_REQUEST,
  payload: {showId},
});

export const fetchSeasonsSuccess = (seasons: number[]) => ({
  type: FETCH_SEASONS_SUCCESS,
  payload: {seasons},
});

export const fetchSeasonsFailure = (error: any) => ({
  type: FETCH_SEASONS_FAILURE,
  payload: {error},
});
