import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch, useSelector} from 'react-redux';
import {fetchSeasonsRequest} from '../../redux/actions/showsActions';
import {AppState} from '../../redux/store';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {HeartStraight} from 'phosphor-react-native';
import {Picker} from '@react-native-picker/picker';
import colors from '../../utils/colors';
import {StackNavigationProp} from '@react-navigation/stack';

const windowWidth = Dimensions.get('window').width - 30 - 150;

const SerieDetailScreen = ({route}: any) => {
  const {show} = route.params;
  const [isFavorite, setIsFavorite] = useState(false);
  const [selectedSeason, setSelectedSeason] = useState(1);
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();
  const seasons = useSelector((state: AppState) => state.shows.seasons);
  useEffect(() => {
    dispatch(fetchSeasonsRequest(show.id));
  }, [dispatch, show.id]);

  useFocusEffect(
    React.useCallback(() => {
      checkIfFavorite();
    }, []),
  );

  const checkIfFavorite = async () => {
    try {
      const favorites = await AsyncStorage.getItem('favorites');
      if (favorites !== null) {
        setIsFavorite(true);
        const parsedFavorites = JSON.parse(favorites);
        const isFavorite_ = parsedFavorites.some(item => item.id === show.id);
        setIsFavorite(isFavorite_);
      } else {
        setIsFavorite(false);
      }
    } catch (error) {
      console.error('Error checking favorites:', error);
    }
  };
  const toggleFavorite = async () => {
    try {
      let favorites = await AsyncStorage.getItem('favorites');
      if (favorites === null) {
        favorites = [];
      } else {
        favorites = JSON.parse(favorites);
      }

      if (isFavorite) {
        favorites = favorites.filter(item => item.id !== show.id);
      } else {
        favorites.push(show);
      }

      await AsyncStorage.setItem('favorites', JSON.stringify(favorites));
      setIsFavorite(!isFavorite);
    } catch (error) {
      console.error('Error toggling favorite:', error);
    }
  };
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <ImageBackground
        source={{uri: show.image.medium}}
        resizeMode="cover"
        style={styles.poster}>
        <LinearGradient
          colors={[
            'rgba(22, 24, 28, 0)',
            'rgba(22, 24, 28, .5)',
            'rgba(22, 24, 28, 1)',
          ]}
          style={styles.linearGradient}>
          <TouchableOpacity onPress={toggleFavorite} style={styles.favorite}>
            {!isFavorite && (
              <Text style={styles.textFavorite}>Add to favorites</Text>
            )}
            <HeartStraight
              color={isFavorite ? 'tomato' : 'rgba(255,255,255,.5)'}
              size={24}
              weight="fill"
            />
          </TouchableOpacity>
          <Text style={styles.title}>{show.name}</Text>
          <View style={styles.detailsContainer}>
            <View style={styles.margin20}>
              <Text style={styles.subtitle}>Transmission Days:</Text>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {show.schedule.days.map((day, index) => (
                  <Text key={index} style={styles.schedule}>
                    {day} {show.schedule.time}
                  </Text>
                ))}
              </ScrollView>
            </View>

            <View style={styles.genres}>
              <Text style={styles.subtitle}>Genders: </Text>
              <Text style={styles.detailItem}>
                {show.genres.length > 0
                  ? show.genres.join(' / ')
                  : 'Without genders'}
              </Text>
            </View>
          </View>
        </LinearGradient>
      </ImageBackground>
      <View style={styles.space}>
        <Text style={styles.subtitle}>Summary:</Text>
        <Text style={styles.summary}>
          {show.summary.replace(/<[^>]+>/g, '')}
        </Text>
      </View>
      <View style={styles.space}>
        <View style={styles.pickerContainer}>
          <Text style={styles.subtitle}>Select season:</Text>
          <Picker
            selectedValue={selectedSeason}
            style={styles.picker}
            onValueChange={itemValue => setSelectedSeason(itemValue)}>
            {seasons &&
              seasons.seasons &&
              seasons.seasons.map((_, index) => (
                <Picker.Item
                  key={index}
                  label={`Season ${index + 1}`}
                  value={index + 1}
                />
              ))}
          </Picker>
        </View>
        <View style={styles.margin20}>
          <Text style={styles.subtitle}>Episodes:</Text>
        </View>
        {seasons && seasons.seasons && seasons.seasons[0] ? (
          seasons.seasons[selectedSeason - 1].map(episode => (
            <TouchableOpacity
              onPress={() => {
                navigation.push('EpisodeDetail', {episode});
              }}
              style={styles.episodeDetail}
              key={episode.id}>
              <View style={styles.rowCenter}>
                {episode.image && episode.image.medium ? (
                  <Image
                    resizeMode="cover"
                    source={{uri: episode.image.medium}}
                    style={styles.thumbnail}
                  />
                ) : (
                  <Image
                    resizeMode="cover"
                    source={require('../../assets/images/image.png')}
                    style={styles.thumbnail}
                  />
                )}
                <View style={styles.textView}>
                  <Text style={styles.font16}>{`${episode.name}`}</Text>
                  <Text style={styles.font12}>{`${episode.airdate}`}</Text>
                  <Text style={styles.font12}>{`${episode.runtime} min`}</Text>
                </View>
              </View>
            </TouchableOpacity>
          ))
        ) : (
          <Text>No hay episodios disponibles.</Text>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderRadius: 5,
    fontFamily: 'Gill Sans',
    fontSize: 18,
    margin: 10,
    padding: 15,
    textAlign: 'center',
    color: colors.white,
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#1d1e24',
    flexGrow: 1,
  },
  detailItem: {
    color: colors.white,
    fontSize: 16,
  },
  detailsContainer: {
    width: '100%',
  },
  episodeDetail: {
    borderBottomColor: '#383838',
    borderBottomWidth: 1,
    marginBottom: 10,
    paddingBottom: 10,
  },
  favorite: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.5)',
    borderRadius: 100,
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 10,
    position: 'absolute',
    right: 20,
    top: 20,
  },
  font12: {
    color: '#d3d3d3',
    fontSize: 12,
  },
  font16: {
    color: colors.white,
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  genres: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 20,
  },
  gradient: {
    flex: 1,
  },
  linearGradient: {
    alignItems: 'flex-start',
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },
  margin20: {
    marginBottom: 20,
  },
  picker: {
    backgroundColor: colors.primary,
    height: 50,
    marginVertical: 10,
    width: '100%',
  },
  pickerContainer: {
    marginTop: 5,
  },
  poster: {
    height: 600,
    resizeMode: 'cover',
    width: '100%',
  },
  rowCenter: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  schedule: {
    backgroundColor: 'rgba(255, 255, 255, .2)',
    borderRadius: 60,
    color: colors.white,
    fontSize: 16,
    marginHorizontal: 5,
    marginVertical: 5,
    marginBottom: 5,
    marginLeft: 0,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  space: {
    padding: 20,
    width: '100%',
  },
  subtitle: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  summary: {
    color: '#d3d3d3',
    fontSize: 16,
    lineHeight: 24,
    marginBottom: 10,
    marginTop: 10,
  },
  textView: {
    paddingHorizontal: 20,
    width: windowWidth,
  },
  thumbnail: {
    backgroundColor: '#25262d',
    height: 80,
    width: 150,
  },
  title: {
    color: colors.white,
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  textFavorite: {
    color: colors.white,
    marginRight: 10,
  },
});

export default SerieDetailScreen;
