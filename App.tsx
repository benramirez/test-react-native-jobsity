import 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/components/screens/HomeScreen';
import SerieDetailScreen from './src/components/screens/SerieDetail';
import EpisodeDetailScreen from './src/components/screens/EpisodeDetail';
import SearchScreen from './src/components/screens/SearchScreen';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {List} from 'phosphor-react-native';
import FavoritesScreen from './src/components/screens/FavoritesScreen';
import colors from './src/utils/colors';
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function DrawerNavigator() {
  return (
    <Drawer.Navigator
      screenOptions={{
        drawerStyle: {
          backgroundColor: colors.drawerBG,
          width: 240,
        },
        drawerActiveBackgroundColor: colors.primary,
        drawerActiveTintColor: 'white',
        drawerInactiveTintColor: 'white',
      }}>
      <Drawer.Screen
        options={{headerShown: false}}
        name="Home"
        component={HomeStack}
      />
      <Drawer.Screen
        options={{headerShown: false}}
        name="Search"
        component={SearchStack}
      />
      <Drawer.Screen
        options={{headerShown: false}}
        name="Favorites"
        component={FavoritesStack}
      />
    </Drawer.Navigator>
  );
}

const MenuButton = ({onPress}: any) => (
  <TouchableOpacity onPress={onPress} style={styles.menu}>
    <List size={32} weight="bold" color="white" />
  </TouchableOpacity>
);

function HomeStack({navigation, route}: any) {
  const renderHeaderRight = () => (
    <MenuButton onPress={() => navigation.openDrawer()} />
  );
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          headerTitleAlign: 'center',
          headerTitle: 'Home',
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="HomeScreen"
        component={HomeScreen}
      />
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          title: route?.params?.show?.name || 'App TV',
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="SerieDetail"
        component={SerieDetailScreen}
      />
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          title: route?.params?.episode.name,
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="EpisodeDetail"
        component={EpisodeDetailScreen}
      />
    </Stack.Navigator>
  );
}

function SearchStack({navigation, route}: any) {
  const renderHeaderRight = () => (
    <MenuButton onPress={() => navigation.openDrawer()} />
  );
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          headerTitleAlign: 'center',
          headerTitle: 'Search',
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="SearchScreen"
        component={SearchScreen}
      />
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          title: route?.params?.show.name,
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="SerieDetail"
        component={SerieDetailScreen}
      />
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          title: route?.params?.episode.name,
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="EpisodeDetail"
        component={EpisodeDetailScreen}
      />
    </Stack.Navigator>
  );
}

function FavoritesStack({navigation, route}: any) {
  const renderHeaderRight = () => (
    <MenuButton onPress={() => navigation.openDrawer()} />
  );
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          headerTitleAlign: 'center',
          headerTitle: 'Favorites',
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="FavoritesScreen"
        component={FavoritesScreen}
      />
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          title: route?.params?.show.name,
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="SerieDetail"
        component={SerieDetailScreen}
      />
      <Stack.Screen
        options={() => ({
          headerRight: renderHeaderRight,
          title: route?.params?.episode.name,
          headerStyle: {
            backgroundColor: colors.primary,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
        name="EpisodeDetail"
        component={EpisodeDetailScreen}
      />
    </Stack.Navigator>
  );
}

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <DrawerNavigator />
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  menu: {
    padding: 20,
  },
});

export default App;
