export default {
  primary: '#ff6e00',
  secondary: '#4a90e2',
  background: '#f0f0f0',
  text: '#333333',
  accent: '#ffcc00',
  white: '#ffffff',
  drawerBG: '#121216',
  backgroundContainer: '#1d1e24',
};
