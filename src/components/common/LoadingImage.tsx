import React, {useState} from 'react';
import {
  Image,
  View,
  ActivityIndicator,
  StyleSheet,
  ImageSourcePropType,
} from 'react-native';
import colors from '../../utils/colors';

interface LoadingImageProps {
  source: ImageSourcePropType;
  style?: any;
}

const LoadingImage: React.FC<LoadingImageProps> = ({source, style}) => {
  const [isLoading, setIsLoading] = useState(true);

  return (
    <View style={[styles.imageContainer, style]}>
      {isLoading && (
        <ActivityIndicator
          style={StyleSheet.absoluteFill}
          size="small"
          color={colors.primary}
        />
      )}
      <Image
        source={source}
        style={[StyleSheet.absoluteFill, style]}
        onLoadEnd={() => setIsLoading(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoadingImage;
