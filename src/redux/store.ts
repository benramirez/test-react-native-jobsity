import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';
import rootSaga from './sagas';
import axios, {AxiosInstance} from 'axios';

export interface AppState {
  shows: ShowsState;
}

export interface ShowsState {
  shows: any[];
  showsPages: any[];
  loading: boolean;
  error: Error | null;
  seasons: any[];
}

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export const axiosInstance: AxiosInstance = axios.create({
  baseURL: 'https://api.tvmaze.com',
  timeout: 5000,
});

export default store;
