import React, {useState} from 'react';
import {View, Text, FlatList, StyleSheet, ImageBackground} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {Trash, TelevisionSimple} from 'phosphor-react-native';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {SortAscending} from 'phosphor-react-native';
import colors from '../../utils/colors';
import {StackNavigationProp} from '@react-navigation/stack';
const FavoritesScreen = () => {
  const [favorites, setFavorites] = useState([]);
  const [originalFavorites, setOriginalFavorites] = useState([]);
  const [isSorted, setIsSorted] = useState(false);
  const navigation = useNavigation<StackNavigationProp<any>>();
  useFocusEffect(
    React.useCallback(() => {
      getFavorites();
    }, []),
  );

  const getFavorites = async () => {
    try {
      const favoritesData = await AsyncStorage.getItem('favorites');
      if (favoritesData !== null) {
        const parsedFavorites = JSON.parse(favoritesData);
        setFavorites(parsedFavorites);
        setOriginalFavorites(parsedFavorites);
      }
    } catch (error) {
      console.error('Error fetching favorites:', error);
    }
  };

  const handleDeleteFavorite = async (id: any) => {
    try {
      const favoritesData = await AsyncStorage.getItem('favorites');
      if (favoritesData !== null) {
        const parsedFavorites = JSON.parse(favoritesData);
        const updatedFavorites = parsedFavorites.filter(
          favorite => favorite.id !== id,
        );
        await AsyncStorage.setItem(
          'favorites',
          JSON.stringify(updatedFavorites),
        );
        setFavorites(updatedFavorites);
      }
    } catch (error) {
      console.error('Error deleting favorite:', error);
    }
  };

  const toggleOrder = (status) => {
    setIsSorted(!isSorted);
    if (!status) {
      const sortedFavorites = [...favorites].sort((a, b) =>
        a.name.localeCompare(b.name),
      );
      setFavorites(sortedFavorites);
    } else {
      setFavorites(originalFavorites);
    }
  };

  const renderFavoriteItem = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.push('SerieDetail', {show: item})}
      style={styles.renderItem}>
      <ImageBackground
        source={
          item.image
            ? {uri: item.image.original}
            : require('../../assets/images/image.png')
        }
        resizeMode="cover"
        style={styles.thumbnail}>
        <LinearGradient
          colors={[
            'rgba(22, 24, 28, 0)',
            'rgba(22, 24, 28, .5)',
            'rgba(22, 24, 28, 1)',
          ]}
          style={styles.linearGradient}>
          <Text style={styles.title}>{item.name}</Text>
          <Text style={styles.detailItem}>
            {item.genres.length > 0 ? item.genres.join(' / ') : 'Sin géneros'}
          </Text>
          <Text
            style={
              styles.seasonText
            }>{`${item.premiered} | ${item.runtime} min `}</Text>
        </LinearGradient>
        <TouchableOpacity
          onPress={() => handleDeleteFavorite(item.id)}
          style={styles.deleteButton}>
          <Trash name="trash" size={24} color={colors.white} />
        </TouchableOpacity>
      </ImageBackground>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      {favorites.length > 0 && (
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => toggleOrder(isSorted)}
            style={styles.button}>
            <Text style={styles.buttonText}>
              {isSorted ? 'Reset order' : 'Sort alphabetically'}
            </Text>
            <View style={styles.sort}>
              <SortAscending color={colors.white} />
            </View>
          </TouchableOpacity>
        </View>
      )}
      {favorites.length > 0 ? (
        <FlatList
          data={favorites}
          renderItem={renderFavoriteItem}
          keyExtractor={(item) => item.id.toString()}
        />
      ) : (
        <View style={styles.empty}>
          <TelevisionSimple color={colors.white} size={40} />
          <Text style={styles.title}>No tienes series favoritas</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonText: {
    color: colors.white,
    fontSize: 15,
    marginRight: 10,
  },
  container: {
    backgroundColor: '#1d1e24',
    flex: 1,
    padding: 20,
    width: '100%',
  },
  deleteButton: {
    bottom: 20,
    position: 'absolute',
    right: 20,
  },
  detailItem: {
    color: colors.white,
    fontSize: 15,
  },
  empty: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    marginBottom: 10,
  },
  linearGradient: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 10,
    width: '100%',
  },
  renderItem: {
    borderRadius: 10,
    marginBottom: 20,
    overflow: 'hidden',
    width: '100%',
  },
  seasonText: {
    color: '#d3d3d3',
    fontSize: 14,
    marginBottom: 5,
  },
  sort: {
    alignItems: 'center',
    backgroundColor: colors.primary,
    borderRadius: 25,
    height: 30,
    justifyContent: 'center',
    width: 30,
  },
  thumbnail: {
    height: 200,
    width: '100%',
  },
  title: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

export default FavoritesScreen;
