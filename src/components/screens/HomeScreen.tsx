import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fetchPageRequest} from '../../redux/actions/showsActions';
import {AppState} from '../../../src/redux/store';
import LoadingImage from '../common/LoadingImage';
import Pagination from '../common/Pagination';
import LoadingFull from '../common/LoadingFull';
import {useNavigation} from '@react-navigation/native';
import colors from '../../utils/colors';
import {StackNavigationProp} from '@react-navigation/stack';

const HomeScreen = () => {
  const navigation = useNavigation<StackNavigationProp<any>>();
  const totalPages = 30;
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const {showsPages, loading, error} = useSelector(
    (state: AppState) => state.shows,
  );

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
      dispatch(fetchPageRequest({page: currentPage - 1}));
    }
  };

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
      dispatch(fetchPageRequest({page: currentPage + 1}));
    }
  };

  useEffect(() => {
    dispatch(fetchPageRequest({page: 1}));
  }, [dispatch]);

  const renderItem = ({item}: any) => {
    return (
      <View style={styles.renderView}>
        <TouchableOpacity
          onPress={() => {
            navigation.push('SerieDetail', {show: item});
          }}
          style={styles.itemContainer}>
          {item.image && (
            <LoadingImage
              source={{uri: item.image.medium}}
              style={styles.itemImage}
            />
          )}
          <Text style={styles.itemText}>{item.name}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <LoadingFull />
      ) : error ? (
        <Text>Error: {error.message}</Text>
      ) : (
        <FlatList
          numColumns={3}
          data={showsPages}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      )}
      <View>
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          handlePrevPage={handlePrevPage}
          handleNextPage={handleNextPage}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.backgroundContainer,
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  input: {
    backgroundColor: colors.white,
    borderColor: 'gray',
    borderRadius: 10,
    borderWidth: 1,
    height: 40,
    marginBottom: 20,
    paddingHorizontal: 10,
    width: '100%',
  },
  itemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemImage: {
    height: '100%',
    width: '100%',
  },
  itemText: {
    backgroundColor: 'rgba(0,0,0,.5)',
    borderRadius: 10,
    bottom: 5,
    color: colors.white,
    fontSize: 10,
    padding: 3,
    position: 'absolute',
    textAlign: 'center',
    zIndex: 10,
  },
  renderView: {
    borderRadius: 10,
    height: 160,
    overflow: 'hidden',
    padding: 2,
    width: '33.3%',
  },
});

export default HomeScreen;
