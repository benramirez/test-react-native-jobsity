import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import colors from '../../utils/colors';

interface PaginationProps {
  currentPage: number;
  totalPages: number;
  handleNextPage: () => void;
  handlePrevPage: () => void;
}

const Pagination: React.FC<PaginationProps> = ({
  currentPage,
  totalPages,
  handleNextPage,
  handlePrevPage,
}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handlePrevPage} disabled={currentPage === 1}>
        <Text style={[styles.button, currentPage === 1 && styles.disabled]}>
          Previous
        </Text>
      </TouchableOpacity>
      <Text style={styles.pageIndicator}>
        Page {currentPage} of {totalPages}
      </Text>
      <TouchableOpacity
        onPress={handleNextPage}
        disabled={currentPage === totalPages}>
        <Text
          style={[
            styles.button,
            currentPage === totalPages && styles.disabled,
          ]}>
          Next
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  button: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    backgroundColor: colors.primary,
    color: colors.white,
    borderRadius: 50,
    marginHorizontal: 5,
    fontSize: 16,
  },
  disabled: {
    opacity: 0.5,
  },
  pageIndicator: {
    fontSize: 14,
    marginHorizontal: 10,
    color: '#a0a0a0',
    fontWeight: 'bold',
  },
});

export default Pagination;
