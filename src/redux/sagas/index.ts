import {all} from 'redux-saga/effects';
// Importa tus sagas individuales aquí
import {watchFetchShows} from './showsSaga';

export default function* rootSaga() {
  yield all([watchFetchShows()]);
}
